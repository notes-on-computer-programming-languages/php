# php

Web development scripting language. https://php.net

[[_TOC_]]

<!--
# Official documentation
...
-->

# New versions
## 8.1
* [*PHP RFC: Enumerations*
  ](https://wiki.php.net/rfc/enumerations)
  2020-12 Larry Garfield, Ilija Tovilo

### Unofficial
* [*PHP 8.1: readonly properties*
  ](https://stitcher.io/blog/php-81-readonly-properties)
  2021-09 Brent
  * Proposed cloning technique is not necessary! It should be avoided!

## 8.0
* [*Attributes overview*](https://www.php.net/manual/fr/language.attributes.overview.php)

### Unofficial
* [*5 New Combos opened by Symfony 5.2 and PHP 8.0*
  ](https://tomasvotruba.com/blog/2020/12/21/5-new-combos-opened-by-symfony-52-and-php-80/)
  2020-12 tomasvotruba
* [*Smooth Upgrade to PHP 8 in Diffs*
  ](https://getrector.org/blog/2020/11/30/smooth-upgrade-to-php-8-in-diffs)
  2020-11 Rector
* [*What's new in PHP 8*
  ](https://stitcher.io/blog/new-in-php-8)
  2020-07 Brent
* [*PHP 8: Constructor property promotion*
  ](https://stitcher.io/blog/constructor-promotion-in-php-8)
  2020-06 Brent@stitcher.io

# Unofficial documentation
## Books
* [*PHP for the Web, Learn PHP without a framework*
  ](https://leanpub.com/learning-php-for-the-web-without-a-framework/)
  2021 Matthias Noback
* [*Advanced Web Application Architecture*
  ](https://leanpub.com/web-application-architecture/c/DECOUPLE_TODAY)
  2020 Matthias Noback
  * [*Book excerpt - Decoupling from infrastructure, Conclusion*
    ](https://matthiasnoback.nl/2021/03/decoupling-from-infrastructure-conclusion/)
    2021-03 Matthias Noback

## Functional Programming in PHP
### Books
* Pro Functional PHP 2017 Rob Aley
* [*Introduction to Functional Programming in PHP*
  ](https://hub.packtpub.com/introduction-functional-programming-php/)
  2016 Gilles Crettenand
* [*Functional PHP*
  ](https://leanpub.com/functional-php)
  *The Art of Function Composition*
  (2016) Luis Atencio

### Blog pages
* [*Functional programming in PHP : Why not ?
An exploration of functional programming techniques with PHP*
  ](https://medium.com/swlh/functional-programming-in-php-why-not-291ded3a3bec)
  2020-05 Steven BOEHM

# Features
## Attribute
* [*Attributes overview*](https://www.php.net/manual/fr/language.attributes.overview.php)

## Generics
* [*State of Generics and Collections*
  ](https://thephp.foundation/blog/2024/08/19/state-of-generics-and-collections/)
  2024-08 Arnaud Le Blanc, Derick Rethans, Larry Garfield (The PHP Foundation)
* [*The case for PHP generics*
  ](https://stitcher.io/blog/generics-in-php-4)
  2022-03 Brent (stitcher.io)

## Network Introduction
* [*PHP Network Functions*
  ](https://www.w3schools.com/php/php_ref_network.asp)

### Cookies
* [*PHP Cookies*
  ](https://www.w3schools.com/php/php_cookies.asp)
* [*$_COOKIE*
  ](https://www.php.net/manual/en/reserved.variables.cookies.php)
* [*HTTP cookie*
  ](https://en.m.wikipedia.org/wiki/HTTP_cookie)

# Test Driven Development
* (fr) [*Le TDD dans la vraie vie avec Panther*
  ](https://blog.steamulo.com/forum-php-2019-jour-2-160da63ee1d)
  2019-11 Sébastien Piquet

# Code analysis
## Documentation
* [*Get Started with PHP Static Code Analysis*
  ](https://deliciousbrains.com/php-static-code-analysis/)
  2021-12 Ross Wintle

## Packages
* 1 <!-- 1 2019-11 -->
  [![PHPPackages Rank
    ](http://phppackages.org/p/phpunit/phpunit/badge/rank.svg)
  ](http://phppackages.org/p/phpunit/phpunit)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpunit/phpunit/badge/referenced-by.svg)](http://phppackages.org/p/phpunit/phpunit)
  phpunit/**phpunit**: The PHP Unit Testing Framework
* 19 <!-- 19 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/friendsofphp/php-cs-fixer/badge/rank.svg)](http://phppackages.org/p/friendsofphp/php-cs-fixer)
  [![PHPPackages Referenced By](http://phppackages.org/p/friendsofphp/php-cs-fixer/badge/referenced-by.svg)](http://phppackages.org/p/friendsofphp/php-cs-fixer)
  friendsofphp/**php-cs-fixer**: Fix PHP code style
* 33 <!-- 33 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpstan/phpstan/badge/rank.svg)](http://phppackages.org/p/phpstan/phpstan)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpstan/phpstan/badge/referenced-by.svg)](http://phppackages.org/p/phpstan/phpstan)
  phpstan/**phpstan** https://gitlab.com/php-packages-demo/composer
* 85 <!-- 85 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpmd/phpmd/badge/rank.svg)](http://phppackages.org/p/phpmd/phpmd)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpmd/phpmd/badge/referenced-by.svg)](http://phppackages.org/p/phpmd/phpmd)
  phpmd/**phpmd**: PHP version of PMD tool
* 87 <!-- 87 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpunit/php-code-coverage/badge/rank.svg)](http://phppackages.org/p/phpunit/php-code-coverage)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpunit/php-code-coverage/badge/referenced-by.svg)](http://phppackages.org/p/phpunit/php-code-coverage)
  phpunit/**php-code-coverage**: Library that provides collection, processing, and rendering functionality for PHP code coverage information
* 139 <!-- 139 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sebastian/phpcpd/badge/rank.svg)](http://phppackages.org/p/sebastian/phpcpd)
  [![PHPPackages Referenced By](http://phppackages.org/p/sebastian/phpcpd/badge/referenced-by.svg)](http://phppackages.org/p/sebastian/phpcpd)
  sebastian/**phpcpd**: Copy/Paste Detector (CPD) for PHP code
* 166 <!-- 166 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phpdocumentor/phpdocumentor/badge/rank.svg)](http://phppackages.org/p/phpdocumentor/phpdocumentor)
  [![PHPPackages Referenced By](http://phppackages.org/p/phpdocumentor/phpdocumentor/badge/referenced-by.svg)](http://phppackages.org/p/phpdocumentor/phpdocumentor)
  phpdocumentor/**phpdocumentor**: Documentation Generator for PHP
* 190 <!-- 190 2020-07 -->
   vimeo/psalm A static analysis tool for finding errors in PHP applications
   * Emphasis on purity and immutability
   * [*Immutability and beyond: verifying program behaviour with Psalm*
     ](https://psalm.dev/articles/immutability-and-beyond)
     2019-10 Matt Brown
* 234 <!-- 234 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/phploc/phploc/badge/rank.svg)](http://phppackages.org/p/phploc/phploc)
  [![PHPPackages Referenced By](http://phppackages.org/p/phploc/phploc/badge/referenced-by.svg)](http://phppackages.org/p/phploc/phploc)
  phploc/**phploc**: A tool for quickly measuring the size of a PHP project
* 338 <!-- 338 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/sensiolabs/security-checker/badge/rank.svg)](http://phppackages.org/p/sensiolabs/security-checker)
  [![PHPPackages Referenced By](http://phppackages.org/p/sensiolabs/security-checker/badge/referenced-by.svg)](http://phppackages.org/p/sensiolabs/security-checker)
  sensiolabs/**security-checker**: A security checker for composer.lock
* 365 <!-- 365 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/pdepend/pdepend/badge/rank.svg)](http://phppackages.org/p/pdepend/pdepend)
  [![PHPPackages Referenced By](http://phppackages.org/p/pdepend/pdepend/badge/referenced-by.svg)](http://phppackages.org/p/pdepend/pdepend)
  pdepend/**pdepend**: PHP version of JDepend
* 531 <!-- 531 2020-07 -->
  phpmetrics/phpmetrics Static analyzer tool for PHP : Coupling, Cyclomatic complexity, Maintainability Index, Halstead's metrics... and more !
  * phpmetrics/symfony-bundle
* 1021 <!-- 1021 2020-07 -->
  kahlan/kahlan The PHP Test Framework for Freedom, Truth and Justice.
  * Nice error messages

# Build tools
* ![Packagist Downloads](https://img.shields.io/packagist/dm/phing/phing)
  [**phing/phing**](https://packagist.org/packages/phing/phing)
  PHing Is Not GNU make; it's a PHP project build system or build tool based on Apache Ant.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/jolicode/castor)
  [**jolicode/castor**](https://packagist.org/packages/jolicode/castor)
  DX oriented task runner and command launcher built with PHP




# Extensions
* [Extension List/Categorization](https://www.php.net/manual/en/extensions.php)

## New
### FFI (PHP 7.4)
* [*PHP RFC: FFI - Foreign Function Interface*](https://wiki.php.net/rfc/ffi)
  2018-12
* [*PHP 7.4 FFI: What you need to know*
  ](https://jolicode.com/blog/php-7-4-ffi-what-you-need-to-know)
  2019-11 Grégoire Pineau

<!--
## Core
## Bundled
-->

## External
### Sodium
* [*Sodium*](https://www.php.net/manual/en/book.sodium.php) PHP Manual
* [*Using Libsodium in PHP Projects*
  ](https://github.com/paragonie/pecl-libsodium-doc)
* [Built in in Debian](https://packages.debian.org/unstable/php)
* [php*-sodium](https://pkgs.alpinelinux.org/packages?name=php*-sodium) in Alpine
* (fr) [*Libsodium pour les nuls, ou la cryptographie en PHP*
  ](https://jolicode.com/blog/libsodium-pour-les-nuls-ou-la-cryptographie-en-php)
  2019-11 Grégoire Pineau

## PECL (Deprecated in PHP ~8)
### Session PgSQL
This extension is considered unmaintained and dead.
* [*PostgreSQL Session Save Handler*
  ](https://www.php.net/manual/en/book.session-pgsql.php)

### Swish
* [*Indexation Swish*](https://www.php.net/manual/fr/book.swish.php)

C library
* [swish-e](https://tracker.debian.org/pkg/swish-e) Debian
* [swish-e](https://pkgs.alpinelinux.org/packages?name=swish-e) Alpine

### UUID
* [PECL](https://pecl.php.net/package/uuid)
* [php-uuid](https://tracker.debian.org/pkg/php-uuid) Debian
* [php*-uuid](https://pkgs.alpinelinux.org/packages?name=php*-uuid) Alpine
* [symfony/polyfill-uuid](https://packagist.org/packages/symfony/polyfill-uuid)
  [![PHPPackages Rank](http://phppackages.org/p/symfony/polyfill-uuid/badge/rank.svg)](http://phppackages.org/p/symfony/polyfill-uuid)
  [![PHPPackages Referenced By](http://phppackages.org/p/symfony/polyfill-uuid/badge/referenced-by.svg)](http://phppackages.org/p/symfony/polyfill-uuid)
* [*UUID generation in PHP*](https://jolicode.com/blog/uuid-generation-in-php)
  2019-11 Grégoire Pineau
* [paragonie/random_compat
  ](https://gitlab.com/php-packages-demo/paragonie-random_compat)
  @ [gitlab.com/php-packages-demo](https://gitlab.com/php-packages-demo)

<!--
## Deprecated
## Experimental
-->

# Libraries
![PHP and PEAR libraries popularity in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=php-psr-log+php-symfony-filesystem+php-psr-container+php-symfony-service-contracts+php-phpseclib+pkg-php-tools+php-composer-ca-bundle+php-twig+php-psr-cache&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)
![PHP and PEAR libraries popularity in Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=php-php-gettext+php-net-socket+jsonlint+php-json-schema+php-net-smtp+php-auth-sasl+php-mail-mime+php-mdb2+php-http-request&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* https://qa.debian.org/developer.php?email=pkg-php-pear%40lists.alioth.debian.org

## Annotations
* [*PHP: Annotations are an Abomination*
  ](https://r.je/php-annotations-are-an-abomination.html)
  2013-01 Tom Butler
  * See PHP ~8

## Authentication
* 230 league/oauth2-client

## Data structures
* [*Efficient data structures for PHP 7*
  ](https://medium.com/@rtheunissen/efficient-data-structures-for-php-7-9dda7af674cd)
  2016-02 Rudi Theunissen

## FastCGI
* zmrd/fastcgi
* garveen/fastcgi-react
* react/fcgi

## Functional programming
### Sources of inspiration for this section
* Pro Functional PHP 2017 Rob Aley [Book]
* Functional PHP 2017 Gilles Crettenand [Book]

### Purity and immutability
* [php immutable](https://google.com/search?q=php+immutable)
* [*PHP 7 performance improvements (5/5): Immutable arrays*
  ](https://blog.blackfire.io/php-7-performance-improvements-immutable-arrays.html)
  2016-12 Julien Pauli
* [*Immutable Objects in PHP*
  ](https://joefallon.net/2015/08/immutable-objects-in-php/)
  2015-08 Joe Fallon
* <!-- 190 2020-07 -->
  [**vimeo/psalm**](https://packagist.org/packages/vimeo/psalm)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/vimeo/psalm/rank.svg)](http://phppackages.org/p/vimeo/psalm)

   A static analysis tool for finding errors in PHP applications
   * Emphasis on purity and immutability
   * [*Immutability and beyond: verifying program behaviour with Psalm*
     ](https://psalm.dev/articles/immutability-and-beyond)
     2019-10 Matt Brown


### Data Structures
* [**tightenco/collect**](https://packagist.org/packages/tightenco/collect)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/tightenco/collect/rank.svg)](http://phppackages.org/p/tightenco/collect)
  Collect - Illuminate Collections (65192) as a separate package.
  * to compare with
    * [**lstrojny/functional-php**](https://packagist.org/packages/lstrojny/functional-php)
      [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/lstrojny/functional-php/rank.svg)](http://phppackages.org/p/lstrojny/functional-php)
    * [**cvek/collection**](https://packagist.org/packages/cvek/collection)
      [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/cvek/collection/rank.svg)](http://phppackages.org/p/cvek/collection)
      Adapter for tightenco/collect bundle to be usable with private and virtual properties in Symfony projects.
    * [**symfony/property-access**](https://packagist.org/packages/symfony/property-access)
      [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/symfony/property-access/rank.svg)](http://phppackages.org/p/symfony/property-access)
* <!-- 7373 2021-09 -->
  [**qaribou/immutable.php**](https://packagist.org/packages/qaribou/immutable.php)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/qaribou/immutable.php/rank.svg)](http://phppackages.org/p/qaribou/immutable.php)
  Immutable, highly-performant collections, well-suited for functional programming and memory-intensive applications.
* <!-- 48651 2021-09 -->
  [**immutablephp/immutable**](https://packagist.org/packages/immutablephp/immutable)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/immutablephp/immutable/rank.svg)](http://phppackages.org/p/immutablephp/immutable)
  Immutable base objects, value objects, and value bag
  * [*Immutable Objects for PHP*
    ](http://paul-m-jones.com/post/2019/02/04/immutability-package-for-php/)
    2019-02 Paul M. Jones
* <!-- 82267 2021-09 -->
  [**lisachenko/immutable-object**](https://packagist.org/packages/immutablephp/immutable)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/lisachenko/immutable-object/rank.svg)](http://phppackages.org/p/lisachenko/immutable-object)
  Immutable object library
  

### Recursion and Trampolines
* 27874 functional-php/trampoline

### Partial Functions and Currying
* 529 lstrojny/functional-php
* 4839 react/partial
* 5636 cypresslab/php-curry
* 11250 phamda/phamda Auto-curried function library

#### Keywords
* curry

### Functional Composition
* 37960 pitchart/transformer A transducers implementation in PHP, with OOP powers

### Monads
* 507 phpoption/phpoption Option Type for PHP
  * Optional lazy evaluation
  * Not sure if it is a monad?
  * Scala option or Haskell Maybe
* 6446 widmogrod/php-functional Functors, Applicative and Monads are fascinating concept. Purpose of this library is to explore them in OOP PHP world.
* 16066 chippyash/monad
* 20288 phunkie/phunkie Functional structures library for PHP
  @[packagist.org](https://packagist.org/packages/phunkie/phunkie)
  @[PHPPackages.org](https://phppackages.org/p/phunkie/phunkie)
* 63913 careship/functional-php A monad-like set of utilities for PHP, type-checked by Psalm.
* 22146 php-fp/php-fp-either
* 81224 php-fp/php-fp-maybe
* 83893 php-fp/php-fp-combinators
* 83926 php-fp/php-fp-writer
* 85939 php-fp/php-fp-reader
* 95986 php-fp/php-fp-state
* 95987 php-fp/php-fp-io

### Functional Framework and router
* [**vlucas/bulletphp**](https://packagist.org/packages/vlucas/bulletphp)
  [![PHPPackages rank](https://pages-proxy.gitlab.io/phppackages.org/vlucas/bulletphp/rank.svg)](http://phppackages.org/p/vlucas/bulletphp)
  A heierarchical resource-oriented micro-framework built on nested closures instead of route-based callbacks

### Underscore, Lodash, Ramda, Scala (Collections) Traversable, and Dojo Framework inspired
<!--
![Packagist Downloads](https://img.shields.io/packagist/dm/)
[](https://packagist.org/packages/)
-->
* ![Packagist Downloads](https://img.shields.io/packagist/dm/lstrojny/functional-php) 529
  [lstrojny/functional-php](https://packagist.org/packages/lstrojny/functional-php)
  Functional primitives for PHP
  * functional primitives (map, pluck, sort, etc.) (to compare with tightenco/collect)
  * maybe not the best for functional composition
* ![Packagist Downloads](https://img.shields.io/packagist/dm/maciejczyzewski/bottomline)
  [maciejczyzewski/bottomline](https://packagist.org/packages/maciejczyzewski/bottomline)
  * A full-on PHP manipulation utility belt that provides support for working with arrays, objects, and iterables; a lodash or underscore equivalent for PHP.
  * Pretends to be fast.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/kapolos/pramda) 12301
  [kapolos/pramda](https://packagist.org/packages/kapolos/pramda)
  Toolkit for Practical Functional Programming in PHP
  * extensive lazy evaluation
  * autocurrying
* ![Packagist Downloads](https://img.shields.io/packagist/dm/mpetrovich/dash)
  [mpetrovich/dash](https://packagist.org/packages/mpetrovich/dash)
  * A functional programming library for PHP. Inspired by Underscore, Lodash, and Ramda.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/bdelespierre/underscore)
  [bdelespierre/underscore](https://packagist.org/packages/bdelespierre/underscore)
  * Underscore.js port in PHP
  * Last update 2015 (development version 2020)
  * Documented
* ![Packagist Downloads](https://img.shields.io/packagist/dm/phamda/phamda) 11250
  [phamda/phamda](https://packagist.org/packages/phamda/phamda)
  Auto-curried function library
  * faster than Pramda
  * more functions than Pramda
* ![Packagist Downloads](https://img.shields.io/packagist/dm/anahkiasen/underscore-php) 744
  [anahkiasen/underscore-php](https://packagist.org/packages/anahkiasen/underscore-php)
  A redacted port of Underscore.js for PHP
  * This package is abandoned and no longer maintained. No replacement package was suggested.
* ![Packagist Downloads](https://img.shields.io/packagist/dm/zeonwang/pamda) 208528
  [zeonwang/pamda](https://packagist.org/packages/zeonwang/pamda)
  function programming lib study
* https://brianhaveri.github.io/Underscore.php/ (Not (no more?) on Packagist)

#### Keywords
* underscore
* lodash
* ramda

### General-Purpose Libraries
* 27497 ikr/fyrfyrfyr Basic tools for composing functions, to write simpler and safer programs
* 78745 bluesnowman/fphp-saber A functional PHP library, which promotes strong typing, immutable objects, and lazy evaluation.
  * To avoid.
* https://github.com/divarvel/PHPZ

#### Keywords
* saber

### Search
* [Functional Programming](https://phppackages.org/s/Functional%20Programming) (PHPPackages.org)

### Keywords
* functional, programming, monad

### Book library Keywords
* Functional PHP

## MVC (Model-Viev-Controller)
* [*The Most Popular PHP Frameworks to Use in 2021*
  ](https://kinsta.com/blog/php-frameworks/#what-are-the-best-php-frameworks-in-2021)
  2021-07 Claire Brotherton
* [*MVC (Model-View-Controller) in PHP tutorial part 1: Hello World*
  ](https://r.je/mvc-in-php.html)
  2010-09 Tom Butler

## ORM
### Cycle
* [cycle/orm](https://github.com/cycle/orm)
* [*comparison to other ORMs*
  ](https://github.com/cycle/docs/issues/3)

## Parser combinator
* [parser combinator library php](https://google.com/search?q=parser+combinator+library+php)
* yay/yay 
  @[packagist.org](https://packagist.org/packages/yay/yay)
  @[PHPPackages.org](https://phppackages.org/p/yay/yay)

## Reactive Programming
* [*PHP Reactive Programming*
  ](https://www.worldcat.org/search?q=ti%3APHP+Reactive+Programming)
  2017 Martin Sikora

## Router
* [router](https://phppackages.org/s/router) (slim, symfony, nikic, klein)

## SQL
### Paginator
* [paginator](https://phppackages.org/s/paginator)
* 24 doctrine/orm Object-Relational-Mapper for PHP
  * Doctrine\ORM\Tools\Pagination
    * [*Pagination*
      ](https://www.doctrine-project.org/projects/doctrine-orm/en/latest/tutorials/pagination.html)
* 180 api-platform/api-platform The API Platform framework
  * [*Pagination*](https://api-platform.com/docs/core/pagination/)
* 257 league/fractal Handle the output of complex data structures ready for API output.
  * [pagination](https://fractal.thephpleague.com/pagination)
* 1018 pagerfanta/pagerfanta
* 5247 [beberlei/porpaginas](https://phppackages.org/p/beberlei/porpaginas)
  * 165828 fightmaster/porpaginas-bundle
* Laravel [Database: Pagination](https://laravel.com/docs/5.8/pagination)
* [symfony Eloquent bundle](https://phppackages.org/s/symfony%20Eloquent%20bundle)

### SQL parser
* [sql parser](https://phppackages.org/s/sql%20parser)
* <!--- greenlion/php-sql-parser -->
  <!--- 1768 2019-11 -->
  [![PHPPackages Rank](http://phppackages.org/p/greenlion/php-sql-parser/badge/rank.svg)](http://phppackages.org/p/greenlion/php-sql-parser)
  [greenlion](https://phppackages.org/s/greenlion)/[php-sql-parser](https://gitlab.com/php-packages-demo/greenlion-php-sql-parser)
  [![PHPPackages Referenced By](http://phppackages.org/p/greenlion/php-sql-parser/badge/referenced-by.svg)](http://phppackages.org/p/greenlion/php-sql-parser)

## String manipulation
* 377 danielstjules/stringy A string manipulation library with multibyte support

## URI manipulation
* 1576 league/uri URI manipulation library

## WebAssembly extensions
* https://pecl.php.net/package/wasm
* https://github.com/wasmerio/php-ext-wasm

## WebSocket
* [*How to create websockets server in PHP*
  ](https://stackoverflow.com/questions/14512182/how-to-create-websockets-server-in-php)
  (2020) Stack Overflow

# Examples
## Ansi colors
php -r 'echo "\e[31mHello\e[0mWorld";'
