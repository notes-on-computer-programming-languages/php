<?php
declare(strict_types=1);

interface iParams
{
    public function first();
    public function second();
}

function echo_interface(iParams $p)
{
    echo $p->first() . ' ' . $p->second() . PHP_EOL;
}

echo_interface(new class implements iParams {
    function second() { return 'two'; }
    function first() { return 'one'; }
});
