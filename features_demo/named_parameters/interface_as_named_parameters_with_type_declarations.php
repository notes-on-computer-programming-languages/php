<?php
declare(strict_types=1);

interface iParams_typed
{
    public function first(): string;
    public function second(): string;
}

function echo_interface(iParams_typed $p): void
{
    echo $p->first() . ' ' . $p->second() . PHP_EOL;
}

echo_interface(new class implements iParams_typed {
    function second(): string { return 'two'; }
    function first(): string { return 'one'; }
});
