<?php
declare(strict_types=1);

function echo_two(string $first, string $second): void
{
    echo $first . ' ' . $second . PHP_EOL;
}

echo_two(second: 'two', first: 'one');
