<?php
declare(strict_types=1);

class BlogData
{
    public function __construct(
        public readonly string $title,
    ) {}

    public function newTitle(
        string $title,
    ): BlogData {return new BlogData($title);}

    public function replaceTitle(
        string $title,
    ): BlogData {return (new BlogData(...array_replace((array) $this, ["title" => $title])));}
    # withTitle should be based on replaceTitle.
}

$dataA = new BlogData('Title');

echo $dataA->title . PHP_EOL;

$dataB = $dataA->newTitle('Another title B');

echo $dataB->title . PHP_EOL;

echo (new BlogData('Title'))->newTitle('With another title')->title . PHP_EOL;

echo (new BlogData('Title'))->replaceTitle('With method replaced title')->title . PHP_EOL;

echo (new BlogData(...array_replace((array) new BlogData('Title'), ["title" => "Replaced title"])))->title . PHP_EOL;

# Inspired by https://stitcher.io/blog/php-81-readonly-properties
